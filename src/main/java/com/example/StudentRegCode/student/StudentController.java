package com.example.StudentRegCode.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController //Makes the following class to serve Rest end points
@RequestMapping(path = "api/v1/student")
public class StudentController {

    private final StudentService studentService; //the constructor is called when an object of a class is created

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping  //The only rest end point that we have here
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

   @GetMapping(path = "{studentId}")
   public Optional<Student> findStudentsById(Long id){
        return studentService.findStudentById(id);
   }

   @GetMapping(path = "{studentName}")
   public Optional<Student> findStudentsByName(Student student){
        return studentService.findStudentsByName(student);
   }


    @PostMapping ("/registerNewStudent")
    public void registerNewStudent(@RequestBody Student student) {
        studentService.addNewStudent(student);
    }

    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(
            @PathVariable("studentID") Long studentId) {
        studentService.deleteStudent(studentId);
    }

    @PutMapping(path = "{studentId}")
    public void updateStudent(
            @PathVariable("studentId") Long studentId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String email) {
        studentService.updateStudent(studentId, name, email);
    }


}
