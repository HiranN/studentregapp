package com.example.StudentRegCode.student;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    public List<Student> getStudents(){
        return studentRepository.findAll(); //This returns a List
    }


    public Optional<Student> findStudentById (Long Id) {

        return studentRepository.findById(Id);
    }

    public Optional<Student> findStudentsByName(Student student) {
        return studentRepository.findStudentByName(student.getName());
    }

    @PostMapping
    public void addNewStudent(Student student) {
        Optional<Student> studentOptional = studentRepository
                .findStudentByEmail(student.getEmail());
        if(studentOptional.isPresent()){
            throw new IllegalStateException("email taken");
        }
        studentRepository.save(student);
    }

    @DeleteMapping
    public void deleteStudent(Long studentId) {
        boolean exists = studentRepository.existsById(studentId);//To check whether if exist in studentRepository
        if (!exists) {
            throw new IllegalStateException(
                    "student with id" + studentId + "does not exists");
        }
        studentRepository.deleteById(studentId);
    }

    @Transactional
    public void updateStudent(Long studentId,
                              String name,
                              String email) {
       Student student = studentRepository.findById(studentId) //Checking whether the student exist with the ID
               .orElseThrow(() -> new IllegalStateException(
                        "student with id " + studentId + " does not exist"));

       if (name != null &&
                name.length() > 0 &&
                !Objects.equals(student.getName(), name)) { //The name provided not the same as the current one
           student.setName(name);//if that's not the case we want to set the name
        }

       if (email != null &&
               email.length() > 0 &&
               !Objects.equals(student.getEmail(), email)) {
           Optional<Student> studentOptional = studentRepository
                   .findStudentByEmail(email);
           if(studentOptional.isPresent()) {
               throw new IllegalStateException("email taken");
           }
           student.setEmail(email);
       }



    }



}
