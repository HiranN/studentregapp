package com.example.StudentRegCode.student;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository
        extends JpaRepository<Student, Long> { //Insert the generic  the type and the id that we want to work with

    //We need a custom function that will find a user by email

    //This transform to a SQL such as "SELECT * FROM student WHERE email = ?"



    @Query("SELECT s FROM Student s WHERE s.email = ?1")
    Optional<Student> findStudentByEmail(String email);

    Optional<Student> findStudentByName(String name);


}


